#!/bin/bash

grcc sstv_testbed_ogg.grc || exit -1

mkdir -p output

for samp in samples_ogg/*.ogg; do
    sampname="$(basename $samp)"
    output="output/${sampname%.*}"
    ./sstv_testbed_ogg.py -i $samp -o $output
done
