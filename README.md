SSTV Testbed
============

Testbed for the satnogs PD120 decoder.

The flowgraphs in the repository are kept as close as possible to the PD120 observation flowgraph
in [gr-satnogs](https://gitlab.com/librespacefoundation/satnogs/gr-satnogs/).
The repo also contains some audio and IQ samoles to test the decoder.

Running the Tests
-----------------
1. Checkout the testbed repo
```
git clone git@gitlab.com:LongHairedHacker/sstv-testbed.git
```

2. Compile and setup gr-satnogs
```
git clone git@gitlab.com:LongHairedHacker/gr-satnogs.git
git checkout  sstv-decoder
cd gr-satnogs
mkdir build
cd build
cmake ..
sudo make install
```

3. Run the flowgraphs
```
cd sstv-testbed
./run_ogg_test.sh
./run_iq_test.sh
```

4. Check the results in the `output` folder

Files
-----
A quick rundown of the different files in this repo.
* `run_iq_test.sh` runs `sstv_testbed_iq.grc` on the samples in `samples_iq`
* `run_ogg_test.sh` runs `sstv_testbed_ogg.grc` on the samples in `samples_ogg`
* `render.py` prototype of the rendering code in the decoder block
* `sstv_demod.grc` prototype of the decoder flowgraph, produces `demod.dat` for render.py
* `sstv_pd120_rtl.grc` Receiver flowgraph for live testing with an rtl-sdr
* `sstv_testbed_iq.grc` flowgraph for demodulating `samples_iq`
* `sstv_testbed_ogg.grc` flowgraph for demodulating `samples_ogg`
* `setup_env.sh` stets up some environment variable to make gnuradio find all the shared object files.
  May be required on some systems.
