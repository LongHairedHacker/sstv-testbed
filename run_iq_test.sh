#!/bin/bash

grcc sstv_testbed_iq.grc || exit -1

mkdir -p output

for samp in samples_iq/*.dat; do
    sampname="$(basename $samp)"
    output="output/${sampname%.*}"
    ./sstv_testbed_iq.py -i $samp -o $output
done
